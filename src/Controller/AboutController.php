<?php

namespace App\Controller;

use App\Repository\PartnersRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AboutController extends BaseController
{
    /**
     * @Route("/about/main", name="app_about")
     */
    public function about(Request $request, PartnersRepository $partnersRepository)
    {
        $partners = $partnersRepository->findAll();

        $this->contactUsHandleForm($request);
        // todo dynamic data for department member
        return $this->render('about/about.html.twig', [
            'partners' => $partners
        ]);
    }

    /**
     * @Route("/about/agricultural-engineering", name="app_history-ae")
     */
    public function equipmentHistory(Request $request)
    {
        $this->contactUsHandleForm($request);
        return $this->render("about/history_ae.html.twig");
    }

    /**
     * @Route("/about/cars-and-tractors", name="app_history-ct")
     */
    public function carsHistory(Request $request)
    {
        $this->contactUsHandleForm($request);
        return $this->render("about/history_ct.html.twig");
    }


    /**
     * @Route("/about/partners", name="app_partners")
     */
    public function ourPartners(Request $request, PartnersRepository $partnersRepository)
    {
        $partners = $partnersRepository->findAll();

        $this->contactUsHandleForm($request);
        // todo getPartners
        return $this->render("about/partners.html.twig", [
            'partners' => $partners
        ]);
    }
}
