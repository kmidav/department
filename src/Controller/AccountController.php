<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserDataFormType;
use App\Repository\NewsRepository;
use App\Service\FileLoader;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AccountController
 * @package App\Controller
 * @IsGranted("ROLE_USER")
 */
class AccountController extends BaseController
{
    /**
     * @Route("/account", name="app_account")
     */
    public function showAccount(LoggerInterface $logger, NewsRepository $newsRepository, Request $request)
    {
        $this->contactUsHandleForm($request);
        $logger->debug('Checking account page for ' . $this->getUser()->getEmail());
        $authors_news = $newsRepository->getAuthorsNews($this->getUser());

        return $this->render('account/account.html.twig', [
            'authors_news' => $authors_news,
        ]);
    }

    /**
     * @Route("account/edit", name="app_account_edit")
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param FileLoader $fileLoader
     * @param $usersPhotoDirectory
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function editAccountInformation(Request $request, EntityManagerInterface $em, FileLoader $fileLoader, $usersPhotoDirectory)
    {
        $userForm = $this->createForm(UserDataFormType::class, $this->getUser());

        $userForm->handleRequest($request);
        if ($userForm->isSubmitted() && $userForm->isValid()) {
            /** @var User $user */
            $user = $userForm->getData();

            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Личные данные отредактированы!');
            return $this->redirectToRoute('app_member_show', [
                'email' => $user->getEmail()
            ]);
        }

        $this->contactUsHandleForm($request);
        return $this->render("account/edit.html.twig", [
            'userForm' => $userForm->createView(),
        ]);
    }
}
