<?php


namespace App\Controller;


use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminUtilityController
 * @package App\Controller
 * @IsGranted("ROLE_ADMIN")
 */
class AdminUtilityController extends AbstractController
{
    /**
     * @Route("admin/utility/users", methods={"GET"}, name="admin_utility_users")
     */
    public function getUsersAutocompleteList(UserRepository $userRepository, Request $request)
    {
        $users = $userRepository->findAllMatchingUsers($request->query->get('query'));

        return $this->json([
            'users' => $users
        ], 200, [], ['groups' => ['main']]);
    }

    /**
     * @Route("admin/panel", name="admin_panel")
     */
    public function showAdminPanel()
    {
        return $this->render('admin/panel.html.twig');
    }

}