<?php


namespace App\Controller;


use App\Entity\User;
use App\Form\ContactUsFormType;
use App\Form\Model\ContactUsFormModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @method User|null getUser()
 */
abstract class BaseController extends AbstractController
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    protected function contactUsHandleForm(Request $request)
    {
        $form = $this->createForm(ContactUsFormType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            /** @var ContactUsFormModel $latterModel */
            $letterModel = $form->getData();
            $message = (new \Swift_Message('Hello Email'))
                ->setFrom($letterModel->email)
                ->setTo('warkorstand@gmail.com')
                ->setBody(
                    $this->renderView(
                    // templates/emails/registration.html.twig
                        'layouts/_contact_message.html.twig',
                        ['name' => $letterModel->name,
                            'message' => $letterModel->message,
                            'email' => $letterModel->email,
                        ]
                    ),
                    'text/html'
                )
            ;

            $this->mailer->send($message);

            $this->addFlash('success', 'Сообщение отправлено!');
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->addFlash('error', 'Сообщение не отправлено! Введите корректные данные!');
        }
    }

    protected function render(string $view, array $parameters = [], Response $response = null): Response
    {
        $parameters += ['contactForm' => $this->getContactUsFormView()];

        return parent::render($view, $parameters, $response);
    }

    private function getContactUsFormView()
    {
        return $this->createForm(ContactUsFormType::class)->createView();
    }


}