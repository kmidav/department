<?php

namespace App\Controller;

use App\Entity\News;
use App\Form\NewsFormType;
use App\Repository\NewsRepository;
use App\Service\FileLoader;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class NewsAdminController
 * @package App\Controller
 */
class NewsAdminController extends BaseController
{
    /**
     * @Route("admin/news/new", name="admin_news_new")
     * @IsGranted("ROLE_AUTHOR")
     */
    public function new(Request $request, EntityManagerInterface $em, FileLoader $fileLoader, $newsImageDirectory)
    {
        $form = $this->createForm(NewsFormType::class, null, [
            'admin_rights' => $this->isGranted("ROLE_ADMIN"),
        ]);
        $form['author']->setData($this->getUser());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var News $news */
            $news = $form->getData();

            /** @var UploadedFile $image */
            $image = $form['image']->getData();

            if ($image) {
                $imageFileName = $fileLoader->upload($image, $newsImageDirectory);
                $news->setImageFilename($imageFileName);
            }

            $news->setAuthor($this->getUser());

            $em->persist($news);
            $em->flush();

            $this->addFlash('success', 'Новая новость добавлена, смотрите список ваших новостей ниже');

            return $this->redirectToRoute('app_account');
        }

        $this->contactUsHandleForm($request);

        return $this->render('news_admin/new.html.twig', [
            'newsForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/admin/news/list", name="admin_news_list")
     * @param NewsRepository $newsRepository
     * @return Response
     * @IsGranted("ROLE_ADMIN")
     */
    public function list(NewsRepository $newsRepository, Request $request)
    {
        $news_list = $newsRepository->findAll();

        $this->contactUsHandleForm($request);
        return $this->render('news_admin/list.html.twig', [
            'news_list' => $news_list,
        ]);
    }


    /**
     * @Route("author/news/{id}/edit", name="author_news_edit")
     * @IsGranted("MANAGE", subject="news")
     */
    public function edit(News $news, Request $request, EntityManagerInterface $em, FileLoader $fileLoader, $newsImageDirectory)
    {
        $form = $this->createForm(NewsFormType::class, $news, [
            'admin_rights' => $this->isGranted("ROLE_ADMIN"),
        ]);

        if ($news->getImageFilename()) {
            $form['image']->setData(new File($newsImageDirectory.'/'.$news->getImageFilename()));
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var UploadedFile $image */
            $image = $form['image']->getData();

            if ($image) {
                $imageFileName = $fileLoader->upload($image, $newsImageDirectory);
                $news->setImageFilename($imageFileName);
            }

            $em->persist($news);
            $em->flush();

            $this->addFlash('success', 'Новость отредактирована, проверьте её ещё раз!');

            return $this->redirectToRoute('news_show', [
                'slug' => $news->getSlug()
            ]);
        }

        $this->contactUsHandleForm($request);
        return $this->render('news_admin/edit.html.twig', [
            'newsForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/author/news/{id}/delete", name="author_news_delete")
     * @IsGranted("MANAGE", subject="news")
     */
    public function delete(News $news, EntityManagerInterface $em)
    {
        $em->remove($news);
        $em->flush();

        $this->addFlash('success', 'Новость успешно удалена!');

        return $this->redirectToRoute('app_account');
    }

}
