<?php


namespace App\Controller;


use App\Entity\News;
use App\Form\ContactUsFormType;
use App\Form\Model\ContactUsFormModel;
use App\Repository\NewsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Cookie;

class NewsController extends BaseController
{
    /**
     * @Route("/", name="app_home")
     * @param NewsRepository $repository
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function homepage(NewsRepository $repository, PaginatorInterface $paginator, Request $request)
    {
        $queryBuilder = $repository->getAllPublishedQueryBuilder();
        $pagination = $paginator->paginate(
            $queryBuilder,
            $request->query->getInt('page', 1),
            6
        );
        $this->contactUsHandleForm($request);

        return $this->render('home/home.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/news/{slug}", name="news_show")
     * @param News $news
     * @return Response
     */
    public function show(News $news, Request $request, EntityManagerInterface $em)
    {
        $this->contactUsHandleForm($request);

        if (!$request->cookies->has($news->getSlug().'views')) {
            $response = new Response();
            $response->headers->setCookie(Cookie::create($news->getSlug().'views', 'viewed', time()+2592000));
            $news->incrementViewsCount();
            $em->flush();
        }

        return $this->render('news/show.html.twig', [
            'news' => $news,
        ], $response ?? null);
    }

    /**
     * @Route("/newslist", name="news_list")
     * @param NewsRepository $repository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    public function show_all(NewsRepository $repository, PaginatorInterface $paginator, Request $request ) {
        $queryBuilder = $repository->getAllPublishedQueryBuilder();
        $pagination = $paginator->paginate(
            $queryBuilder,
            $request->query->getInt('page', 1),
            9
        );

        $this->contactUsHandleForm($request);
        return $this->render('news/list.html.twig', [
            'pagination' => $pagination,
        ]);
    }
}