<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class StaffController extends BaseController
{
    /**
     * @Route("/staff", name="app_staff_list")
     * @param UserRepository $userRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function staffList(UserRepository $userRepository, Request $request)
    {
        $this->contactUsHandleForm($request);
        $staff = $userRepository->findAllStaff();
        return $this->render('staff/list.html.twig', [
            'staff' => $staff,
        ]);
    }

    /**
     * @Route("/curators", name="app_curators")
     */
    public function curatorsList(Request $request)
    {
        $this->contactUsHandleForm($request);
        return $this->render("staff/curators.html.twig");
    }

    /**
     * @Route("/staff/{email}", name="app_member_show")
     */
    public function showMember(User $user, Request $request)
    {
        $this->contactUsHandleForm($request);
        return $this->render("staff/member.html.twig", [
            'user' => $user
        ]);
    }

    /**
     * @Route("/research-and-evelopment", name="app_niokr")
     */
    public function showNiokrInformation(Request $request)
    {
        $this->contactUsHandleForm($request);
        return $this->render("staff/niokr.html.twig");
    }
}
