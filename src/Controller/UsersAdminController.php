<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserDataFormType;
use App\Repository\UserRepository;
use App\Service\FileLoader;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UsersAdminController
 * @package App\Controller
 * @IsGranted("ROLE_ADMIN")
 */
class UsersAdminController extends BaseController
{
    /**
     * @Route("/admin/users/list", name="admin_users_list")
     */
    public function list(UserRepository $userRepository)
    {
        $users = $userRepository->findAll();

        return $this->render('users_admin/list.html.twig', [
            'users' => $users
        ]);
    }

    /**
     * @Route("admin/{email}/edit", name="admin_user_edit")
     * @param User $user
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function edit(User $user, Request $request, EntityManagerInterface $em, FileLoader $fileLoader, $usersPhotoDirectory) {

        $userForm = $this->createForm(UserDataFormType::class, $user, [
            'admin_rights' => true,
        ]);

        $userForm->handleRequest($request);
        if ($userForm->isSubmitted() && $userForm->isValid()) {
            /** @var User $user */
            $user = $userForm->getData();
            $photo = $userForm['photo']->getData();

            if ($photo) {
                $imageFileName = $fileLoader->upload($photo, $usersPhotoDirectory);
                $user->setPhotoFilename($imageFileName);
            }

            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Личные данные отредактированы!');
            return $this->redirectToRoute('app_member_show', [
                'email' => $user->getEmail()
            ]);
        }

        $this->contactUsHandleForm($request);
        return $this->render("users_admin/edit.html.twig", [
            'userForm' => $userForm->createView(),
        ]);
    }
}
