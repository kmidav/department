<?php

namespace App\DataFixtures;

use App\Entity\News;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class NewsFixtures extends BaseFixture implements DependentFixtureInterface
{
    private static $newsTitles = [
        'Метод расчета условий разрушения материала',
        'Компьютерная модель динамического состояния ',
        'Повышение надёжности автосамосвалов',
        'Устранение остаточного перегиба',
    ];
    private static $newsImages = [
        'product-construction-370x230.jpg',
        'product-organic-370x230.jpg',
        'products-king-news-370x230.jpg',
    ];

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(30, 'main_news', function($count) use ($manager) {
            $news = new News();
            $news->setTitle($this->faker->randomElement(self::$newsTitles))
                ->setContent(<<<EOF
Spicy **jalapeno bacon** ipsum dolor amet veniam shank in dolore. Ham hock nisi landjaeger cow,
lorem proident [beef ribs](https://baconipsum.com/) aute enim veniam ut cillum pork chuck picanha. Dolore reprehenderit
labore minim pork belly spare ribs cupim short loin in. Elit exercitation eiusmod dolore cow
**turkey** shank eu pork belly meatball non cupim.

Laboris beef ribs fatback fugiat eiusmod jowl kielbasa alcatra dolore velit ea ball tip. Pariatur
laboris sunt venison, et laborum dolore minim non meatball. Shankle eu flank aliqua shoulder,
capicola biltong frankfurter boudin cupim officia. Exercitation fugiat consectetur ham. Adipisicing
picanha shank et filet mignon pork belly ut ullamco. Irure velit turducken ground round doner incididunt
occaecat lorem meatball prosciutto quis strip steak.

Meatball adipisicing ribeye bacon strip steak eu. Consectetur ham hock pork hamburger enim strip steak
mollit quis officia meatloaf tri-tip swine. Cow ut reprehenderit, buffalo incididunt in filet mignon
strip steak pork belly aliquip capicola officia. Labore deserunt esse chicken lorem shoulder tail consectetur
cow est ribeye adipisicing. Pig hamburger pork belly enim. Do porchetta minim capicola irure pancetta chuck
fugiat.
EOF
            );

            // publish most articles
            if ($this->faker->boolean(70)) {
                $news->setPublishedAt($this->faker->dateTimeBetween('-100 days', '-1 days'));
            }

//            $news->setAuthor($this->getRandomReference('main_users'))
            $news->setAuthor($this->getRandomReference('main_users'))
                ->setHeartCount($this->faker->numberBetween(5, 100))
                ->setImageFilename($this->faker->randomElement(self::$newsImages))
            ;

            $tags = $this->getRandomReferences('main_tags', $this->faker->numberBetween(0, 3));
            foreach ($tags as $tag) {
                $news->addTag($tag);
            }

            return $news;
        });

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixture::class,
            TagFixture::class,
        ];
    }
}
