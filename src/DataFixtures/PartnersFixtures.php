<?php


namespace App\DataFixtures;


use App\Entity\Partner;
use Doctrine\Common\Persistence\ObjectManager;

class PartnersFixtures extends BaseFixture
{
    private static $paths = [
        'client-01-169x68.png',
        'client-02-126x68.png'
    ];

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(10, 'main_partners', function() {
            $partner = new Partner();
            $partner->setName($this->faker->realText(20))
                ->setLink($this->faker->url)
                ->setLogoFilename($this->faker->randomElement(self::$paths));

            return $partner;
        });

        $manager->flush();
    }

}