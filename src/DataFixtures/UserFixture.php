<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends BaseFixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    private static $photoFilenames = [
        'user-alex-merphy-270x270.jpg',
        'user-amanda-smith-270x270.jpg',
        'user-bernard-show-270x270.jpg',
        'user-diana-russo-270x270.jpg',
        'user-eugene-newman-270x270.jpg',
        'user-john-doe-270x270.jpg',
        'user-sam-cole.jpg',
        'user-july-mao-270x270.jpg',
    ];

    private static $post = [
        'ЗАВЕДУЮЩИЙ КАФЕДРОЙ',
        'ЗАМЕСТИТЕЛЬ ЗАВЕДУЮЩЕГО КАФЕДРОЙ, ДОЦЕНТ',
        'ЗАВЕДУЮЩИЙ ЛАБОРАТОРИЕЙ',
        'ПРОФЕССОР',
        'ДОЦЕНТ',
        'СПЕЦИАЛИСТ ПО УЧЕБНО-МЕТОДИЧЕСКОЙ РАБОТЕ',
        'ИНЖЕНЕР'
    ];

    private static $academicDegrees = [
      'доктор теxнических наук, доцент',
      'кандидат технических наук, доцент',
       'кандидат технических наук',
    ];

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function loadData(ObjectManager $manager)
   {
       $this->createMany(14, 'main_users', function ($i) {
          $user = new User();
          $user->setEmail(sprintf('author%d@ex.com', $i))
              ->setFirstName($this->faker->name)
              ->setRoles(['ROLE_AUTHOR'])
              ->setPhotoFilename($this->faker->randomElement(self::$photoFilenames))
              ->setPhoneNumber($this->faker->phoneNumber)
              ->setPosition($this->faker->streetAddress)
              ->setAcademicDegree($this->faker->randomElement(self::$academicDegrees))
              ->setPost($this->faker->randomElement(self::$post))
              ->setDescription($this->faker->text(6000))
              ->agreeTerms()
              ->setPassword($this->passwordEncoder->encodePassword(
                  $user,
                  '0220'
              ));


          return $user;
       });

       $this->createMany(3, 'admin_users', function ($i) {
           $user = new User();
           $user->setEmail(sprintf('admin%d@ex.com', $i))
               ->setFirstName($this->faker->firstName)
               ->agreeTerms()
               ->setPhotoFilename($this->faker->randomElement(self::$photoFilenames))
               ->setRoles(['ROLE_ADMIN'])
               ->setPost($this->faker->randomElement(self::$post))
               ->setPassword($this->passwordEncoder->encodePassword(
                   $user,
                   '0220'
               ));


           return $user;
       });

       $manager->flush();
   }
}
