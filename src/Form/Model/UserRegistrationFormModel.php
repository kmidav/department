<?php


namespace App\Form\Model;


use App\Validator\UniqueUser;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

class UserRegistrationFormModel
{
    /**
     * @Assert\NotBlank(message="Введите email!")
     * @Assert\Email(message="Введите корректный email!")
     * @UniqueUser()
     */
    public $email;

    /**
     * @Assert\NotBlank(message="Введите имя!")
     */
    public $firstName;

    /**
     * @Assert\NotBlank(message="Введите пароль!")
     * @Assert\Length(min="5", minMessage="Пароль не может быть короче 6 символов!")
     */
    public $plainPassword;
    /**
     * @Assert\IsTrue(message="Вы должны согласиться с правилами!")
     */
    public $agreeTerms;
}