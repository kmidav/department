<?php


namespace App\Form;


use App\Entity\News;
use App\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class NewsFormType extends AbstractType
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'help' => 'Выберите что нибудь оригинальное!',
            ])
            ->add('content')

            ->add('author', UserSelectTextType::class, [
                'disabled' => !$options['admin_rights'],
            ]);

            if ($options['admin_rights']) {
                $builder->add('image', FileType::class,[
                    'mapped' => false,
                    'required' => true,
                    'constraints' => [
                        new NotBlank(["message" => 'У новости обязана быть картинка!'])
                    ]
                ])
                ->add('publishedAt', null, [
                    'widget' => 'single_text',
                ])
                ->add('tags');

            } else {
                $builder->add('image', FileType::class,[
                    'mapped' => false,
                    'required' => false,
                ]);
            }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data_class' => News::class,
            'admin_rights' => false,
        ]);
    }


}