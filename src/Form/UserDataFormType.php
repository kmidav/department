<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserDataFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('firstName')
            ->add('phoneNumber')
            ->add('description')
            ->add('position')
            ;
        if ($options['admin_rights']) {
            $builder->add('post')
                ->add('academicDegree')
                ->add('roles', ChoiceType::class, [
                    'choices' => [
                      'admin' => 'ROLE_ADMIN',
                      'author' => 'ROLE_AUTHOR',
                    ],
                    'multiple' => true,
                ])
                ->add('photo', FileType::class,[
                    'mapped' => false,
                    'required' => true,
                ])
            ;
        }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'admin_rights' => false,
        ]);
    }
}
