<?php


namespace App\Service;


use App\Helper\LoggerTrait;
use Nexy\Slack\Client;

class SlackClient
{
    use LoggerTrait;

    public function sendMessage(string $from, string $message)
    {
        $this->logInfo('Hey what\'s up?', [
            'message' => $message,
        ]);
    }
}